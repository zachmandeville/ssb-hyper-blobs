const test = require('tape')

const path = require('path')
const fs = require('fs')
const Server = require('../testbot')
const { promisify: p } = require('util')
const defaults = require('../../lib/defaults').autoPrune

test('ssb.hyperBlobs.autoPrune.set + ssb.hyperBlobs.autoPrune.get', async t => {
  const alice = Server({
    hyperBlobs: {
      autoPrune: {
        startDelay: 4000
      }
    }
  })

  const set = p(alice.hyperBlobs.autoPrune.set)
  const get = p(alice.hyperBlobs.autoPrune.get)

  /* Valididate config */
  let result

  result = await set(true)
  t.equal(result, undefined, 'can set to true')
  result = await get()
  t.deepEqual(result, defaults, 'if auto-prune is true, it has default config')

  result = await set(false)
  t.equal(result, undefined, 'can set to false')
  result = await get()
  t.equal(result, null, 'if auto-prune is false, it has no config')

  result = await set({ maxRemoteSize: 500 })
  t.equal(result, undefined, 'can set to { maxRemoteSize }')
  result = await get()
  t.deepEqual(result, { ...defaults, maxRemoteSize: 500 }, 'contains default + set config')

  result = await set('dog')
    .catch(err => err)
  t.match(result.message, /invalid autoPrune config/, 'cannot set to string')

  /* Persist config */
  const config = { maxRemoteSize: 5000, intervalTime: 100000000 }
  await set(config)

  t.deepEqual(alice.config.hyperBlobs.autoPrune, config, 'mutates ssb.config.hyperBlobs.autoPrune')

  result = await get()
  t.deepEqual(result, { ...defaults, ...config }, 'contains default + set config')

  const configPath = path.join(alice.config.path, 'config')
  const configFile = JSON.parse(fs.readFileSync(configPath, 'utf8'))
  t.deepEqual(configFile.hyperBlobs.autoPrune, config, 'is persisted to /{appHome}/config')

  alice.close()
  t.end()
})
