const test = require('tape')

const path = require('path')
const pull = require('pull-stream')
const file = require('pull-file')

const fs = require('fs')
const axios = require('axios')

const Server = require('./testbot')
const toURL = require('../data-to-url')
const filePath = path.resolve(__dirname, '../README.md')

test('hyperBlobs.add', t => {
  const server = Server()
  const port = server.config.hyperBlobs.port

  pull(
    file(filePath),
    server.hyperBlobs.add((err, data = {}) => {
      t.error(err, 'adds file')

      axios.get(toURL(data, { port }))
        .then(res => {
          const fileContent = fs.readFileSync(filePath, 'utf-8')
          t.deepEqual(res.data, fileContent, 'retrieve file over http!')

          server.close(t.end)
        })
    })
  )
})
