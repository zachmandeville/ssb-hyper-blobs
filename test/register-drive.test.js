const test = require('tape')
const crypto = require('crypto')

const { connect } = require('scuttle-testbot')
const Server = require('./testbot')

test('hyperBlobs.registerDrive (hits store)', t => {
  const alice = Server()
  const address = '13e900f3449dfb22f855be96fbe6ecb93364d458974c42851ee5620fdff2e42e'

  alice.hyperBlobs.registerDrive(address, (err) => {
    t.error(err, 'registers an address with no problems')

    alice.hyperBlobs.onReady((_, artefactServer) => {
      const hasDrive = (
        artefactServer.store.drives.remote.has(address) ||
        artefactServer.store.drives.loading.has(address)
      )

      t.true(hasDrive, 'alice has registered the drive down in artefact-store')

      alice.close()
      t.end()
    })
  })
})

test('hyperBlobs.registerDrive (2 peers)', t => {
  const registered = new Set()
  const isFinished = () => registered.size === 2
  const caps = {
    shs: crypto.randomBytes(32).toString('base64')
  }

  let bob

  const alice = Server({
    caps,
    hyperBlobs: {
      port: 61001
    }
  })
  alice.hyperBlobs.registerDrive.hook(function (registerDrive, args) {
    const [address, cb] = args // eslint-disable-line
    bob.hyperBlobs.driveAddress((err, bobDrive) => {
      if (err) throw err

      t.equal(address, bobDrive, 'alice registers bobs driveAddress')
      next(alice.id + bobDrive)
      // registerDrive.apply(this, args)
    })
  })

  // NOTE here we simulate alice being completely ready
  // (has been running for 10 seconds, so hyperdrive completely ready)
  // while bob is still starting his systems up when they initially connect.
  //
  // this puts the system in a state where some RPC things might timeout while driveAddress
  // is determined, which might force us to have to reconnect to ensure drive is correctly registered
  const DELAY = 10e3

  setTimeout(() => {
    bob = Server({
      caps,
      hyperBlobs: {
        port: 61002
      }
    })

    bob.hyperBlobs.registerDrive.hook(function (registerDrive, args) {
      const [address, cb] = args // eslint-disable-line
      alice.hyperBlobs.driveAddress((err, aliceDrive) => {
        if (err) throw err

        t.equal(address, aliceDrive, 'bob registers alices driveAddress')
        next(bob.id + aliceDrive)
        // registerDrive.apply(this, args)
      })
    })

    connect([alice, bob], {}, (err) => {
      if (err) throw err
    })
  }, DELAY)

  function next (id) {
    registered.add(id)

    if (isFinished()) {
      alice.close()
      bob.close()
      t.end()
    }
  }
})

test('hyperBlobs.registerDrive (1 stranger, 1 pataka)', t => {
  t.plan(2)
  const caps = {
    shs: crypto.randomBytes(32).toString('base64')
  }

  let failsCount = 0
  const pataka = Server({
    caps,
    hyperBlobs: {
      pataka: true, // <<<< pataka!
      port: 61001
    }
  })
  pataka.hyperBlobs.registerDrive.hook(function (registerDrive, args) {
    t.fail('should not have registered any drives!')
    failsCount++
  })
  pataka.friends.isFollowing.hook(function (isFollowing, args) {
    const [input, cb] = args
    t.deepEqual(input, { source: pataka.id, dest: bob.id }, 'pataka checks its friends') // test #1

    isFollowing.call(this, input, cb)
    startEnd()
  })

  const bob = Server({
    caps,
    hyperBlobs: {
      port: 61002
    }
  })
  bob.hyperBlobs.registerDrive.hook(function (registerDrive, args) {
    t.fail('bob should not register pataka hyperdrive (does not exist)')
    failsCount++
  })

  connect([bob, pataka], {}, (err) => {
    if (err) throw err
  })

  function startEnd () {
    setTimeout(
      () => {
        t.equal(failsCount, 0, 'no drives are registered') // test #2
        pataka.close()
        bob.close()
        t.end()
      },
      3000
    )
  }
})

test('hyperBlobs.registerDrive (1 pataka friend, 1 pataka)', t => {
  const caps = {
    shs: crypto.randomBytes(32).toString('base64')
  }

  const pataka = Server({
    caps,
    hyperBlobs: {
      pataka: true, // <<<< pataka!
      port: 61003
    }
  })
  let callCounter = 0
  pataka.hyperBlobs.registerDrive.hook(function (registerDrive, args) {
    callCounter++
    const [incomingAddress, cb] = args // eslint-disable-line
    bob.hyperBlobs.driveAddress((_, bobAddress) => {
      t.equal(incomingAddress, bobAddress, 'pataka registers bob drive')

      if (callCounter === 1) startEnd()
    })
  })

  const bob = Server({
    caps,
    hyperBlobs: {
      port: 61004
    }
  })
  bob.hyperBlobs.registerDrive.hook(function (registerDrive, args) {
    t.fail('bob should not register the pataka adresss (does not exist)')
  })

  pataka.friends.follow(bob.id, {}, (err) => {
    t.error(err, 'pataka follows bob')

    console.time('time-to-register')
    connect([bob, pataka], {}, (err) => {
      if (err) throw err
    })
  })

  function startEnd () {
    console.timeEnd('time-to-register')
    setTimeout(() => { // we wait a second to see if any other registration happen
      t.equal(callCounter, 1, 'pataka only recorded one registerDrive call')
      pataka.close()
      bob.close()
      t.end()
    }, 1e3)
  }
})
