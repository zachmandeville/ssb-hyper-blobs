module.exports = function IsPrunableFile (opts = {}) {
  if (typeof opts !== 'object') throw new Error('opts given needs to be an object')

  const { minSize = 0, maxSize = null, minDate = 0, maxDate = null, pruneSize = null } = opts

  if (!isValid(pruneSize, [isNull, isNumber])) throw new Error('opts.pruneSize wasnt a valid number. Value given: ' + pruneSize)

  if (!isValid(minSize, [isZero, isNumber])) throw new Error('opts.minSize wasnt a valid number. Value given: ' + minSize)
  if (!isValid(maxSize, [isNull, isNumber])) throw new Error('opts.maxSize wasnt a valid number. Value given: ' + maxSize)

  if (!isValid(minDate, [isZero, isNumber, isDate])) throw new Error('opts.minDate wasnt a number or date. Value given: ' + minDate)
  if (!isValid(maxDate, [isNull, isNumber, isDate])) throw new Error('opts.maxDate wasnt a number or date. Value given:' + maxDate)

  let curPruneSize = 0

  return function isPrunableFile (file) {
    if (pruneSize !== null) {
      // skip if we've already reached pruneSize
      if (curPruneSize >= pruneSize) return false

      // skip if we would exceed the pruneSize with this file
      if (curPruneSize + file.size > pruneSize) return false
    }

    // check this file meets the size and date constraints
    if (!isBetween(minSize, maxSize, file.size)) return false
    if (!isBetween(minDate, maxDate, file.atime)) return false

    curPruneSize += file.size

    return true
  }
}

const isBetween = (lower, upper, value) => {
  return (
    value >= lower &&
    ((upper === null) || (value <= upper))
  )
}

function isValid (value, tests) {
  return tests.some(test => test(value))
}

function isNumber (n) {
  // NOTE technically this is isPositiveNumber
  return typeof n === 'number' && n >= 0
}

function isZero (n) {
  return n === 0
}

function isNull (n) {
  return n === null
}

function isDate (d) {
  return d instanceof Date
}
