module.exports = function ({ driveAddress, blobId, readKey, mimeType }, opts = {}) {
  const {
    port = 26836,
    fileName
  } = opts

  const url = new URL(`http://localhost:${port}/drive/${hexify(driveAddress)}/blob/${blobId}`)
  url.search = new URLSearchParams(prune({
    readKey: hexify(readKey),
    fileName,
    mimeType
  }))

  return url.href
}

function hexify (str) {
  if (str.length === 64) return str // a 32 byte key in hex

  if (!str.endsWith('=')) throw new Error(`unknown string... expected hex / base64, got ${str}`)

  return Buffer.from(str, 'base64').toString('hex')
}

function prune (obj) {
  for (const key in obj) {
    if (obj[key] == null) delete obj[key]
  }

  return obj
}
