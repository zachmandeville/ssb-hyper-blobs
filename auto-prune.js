const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const defaults = require('./lib/defaults').autoPrune

module.exports = function autoPrune (ssb, artefactServerObz) {
  let config = (ssb.config.hyperBlobs && ssb.config.hyperBlobs.autoPrune)
  if (!config) return () => {} // abortAutoPrune noop

  let isClosing = false
  let isPruning = false
  let timeout
  let interval

  ssb.close.hook((close, args) => {
    const _this = this

    isClosing = true
    clearTimeout(timeout)
    clearInterval(interval)
    close.apply(_this, args)
  })
  // NOTE - each time this autoPrune is set it hooks close
  // this happens each time a person calls autoPrune.set, which isn't ideal,
  // but is probably fine as they won't be calling it often

  artefactServerObz.once(artefactServer => {
    if (isClosing) return

    if (config === true) config = {}
    const {
      startDelay,
      intervalTime,
      maxRemoteSize
    } = { ...defaults, ...config }

    const prune = buildPrune(artefactServer, maxRemoteSize, (err, totalPruned) => {
      if (err) console.error('ssb-hyper-blobs: error occurred in auto-prune', err)
      else console.log(`ssb-hyper-blobs: auto-pruned ${totalPruned} bytes`)
    })

    timeout = setTimeout(
      () => {
        prune()
        interval = setInterval(prune, intervalTime)
      },
      startDelay
    )
  })

  function buildPrune (artefactServer, maxRemoteSize, cb) {
    return function doPrune () {
      if (isClosing) return
      if (isPruning) return cb(new Error('pruning already underway, skipping runPrune'))
      isPruning = true

      getTotalSize(artefactServer, (err, totalSize) => {
        if (err) return cb(err)

        const pruneSize = totalSize - maxRemoteSize
        if (pruneSize <= 0) {
          isPruning = false
          cb(null, 0)
          return
        }

        ssb.hyperBlobs.prune({ pruneSize }, (err, prunedFiles) => {
          isPruning = false

          if (err) {
            return cb(err)
          }

          const totalPruned = prunedFiles.reduce((acc, file) => acc + file.size, 0)

          cb(null, totalPruned)
        })
      })
    }
  }

  return function abortAutoPrune () {
    clearTimeout(timeout)
    clearInterval(interval)
  }
}

function getTotalSize (artefactServer, cb) {
  pull(
    pullDriveSizes(artefactServer),
    pull.collect((err, sizes) => {
      if (err) return cb(err)

      const totalSize = sizes.reduce((a, b) => a + b, 0)
      cb(null, totalSize)
    })
  )
}

function pullDriveSizes (artefactServer) {
  return pull(
    pull.values(artefactServer.store.remoteDrives),
    pull.map(drive => drive.key),
    paraMap((driveAddress, cb) => {
      artefactServer.store.driveSize(driveAddress)
        .catch(err => cb(err))
        .then(size => cb(null, size))
    }, 6)
  )
}
